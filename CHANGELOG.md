# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.2] - 2018-11-12

### Added

- License file of AGPL 3.0.

### Fixed

- File ID should continue from last downloaded ID, not start from 1 again.

## [0.1.1] - 2018-11-10

### Added

- Changelog.

### Changed

- Rename. NPM doesn't allow the original package name.

## 0.1.0 - 2018-11-10

### Added

- Command line tool to download instagram post and all posts of a user.
- Also possible to use it as a node module.
- Save download data so it won't download existing photos again. This makes it
  able to keep synchronization with an online account.

[Unreleased]: https://github.com/olivierlacan/keep-a-changelog/compare/v0.1.2...HEAD
[0.1.2]: https://github.com/olivierlacan/keep-a-changelog/compare/v0.1.2...v0.1.1
[0.1.1]: https://github.com/olivierlacan/keep-a-changelog/compare/v0.1.1...v0.1.0
