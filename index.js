#! /usr/bin/env node

const puppeteer = require('puppeteer');
const srcset = require('srcset');
const fs = require('fs');
const request = require('request');

function download(uri, filename) {
    request(uri).pipe(fs.createWriteStream(filename));
};

function sleep(millis) {
    return new Promise(resolve => setTimeout(resolve, millis));
}

/**
 * Fetch Instagram Photos
 * @param {String} url Instagram URL of a post page, a user page, @username
 * @param {Object} options
 * @param {Object[]} options.posts Posts shouldn't be downloaded again
 * @param {Object[]} options.photos Photos shouldn't be downloaded again
 * @param {Boolean} options.download Whether download photos or just return URLs
 */
async function downloadInstagramPhotos(url, options) {
    const functionStart = new Date();

    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    let posts = options && options.posts ? options.posts : [];
    let photos = options && options.photos ? options.photos : [];

    const lastPostUrl = posts.length ? posts[posts.length - 1].url : '';

    if (url.indexOf('https://') === 0) {
        if (url.indexOf('https://www.instagram.com/p/') === 0) {
            await downloadPost(url);
        } else {
            await downloadUser(url);
        }
    } else {
        if (url.indexOf('@') === 0) {
            await downloadUser('https://www.instagram.com/' + url.substr(1));
        } else {
            await downloadPost('https://www.instagram.com/p/' + url);
        }
    }

    await browser.close();

    const functionEnd = new Date();

    const functionDuration = (functionEnd - functionStart) / 1000;

    console.log('Downloaded: ' + photos.length + ' photos');
    console.log('Time usage: ' + (functionDuration / 60).toFixed(0) + ':' + (functionDuration % 60).toFixed(0));

    return {
        url,
        lastDownload: functionStart.toISOString(),
        posts,
        photos
    };

    async function downloadUser(url) {
        await page.goto(url);

        const postDict = {};

        async function getNewPosts(times = 0) {
            const postUrls = await page.evaluate(() =>
                Array.from(document.getElementsByTagName('a'))
                    .map(a => a.href)
                    .filter(h => h.indexOf('https://www.instagram.com/p/') === 0)
            );

            for (let i = 0; i < postUrls.length; i++) {
                const postUrl = postUrls[i];
                if (lastPostUrl === postUrl) {
                    return false;
                }
                postDict[postUrl] = 1;
            }

            // Detect if page has reach end
            const isEnd = await page.evaluate(() =>
                window.scrollY + window.innerHeight >= document.body.scrollHeight
            );

            if (!isEnd) {
                return true;
            } else if (times < 30) {
                // Retry 30 times, total duration is 3 seconds
                await sleep(100);
                return await getNewPosts(times + 1);
            } else {
                return false
            }
        }

        while (await getNewPosts()) {
            await page.keyboard.press('PageDown');
        };

        const newPosts = Object.keys(postDict).reverse().map(url => ({ url, photos: [] }));

        console.log('Found ' + newPosts.length + ' new posts');

        const idBase = posts.length; // id continues from existing posts

        for (let i = 0; i < newPosts.length; i++) {
            await downloadPost(newPosts[i].url, i + 1 + idBase);
        }
    }

    async function downloadPost(url, saveId) {
        if (lastPostUrl && lastPostUrl.indexOf(url) > -1) {
            return;
        }

        let post = posts.find(p => p.url.indexOf(url) > -1);

        if (!post) {
            post = { url, photos: [] };
            posts.push(post);
        }

        await page.goto(url);

        // Find all <img srcset="..." ... />
        const srcsets = await page.evaluate(() =>
            // Browser environment, no Node.js packages can be used here
            Array.from(document.getElementsByTagName('img'))
                .filter(img => img.srcset)
                .map(img => img.srcset)
        ).catch(e => {
            console.log(e);
        });

        // Parse srcset properties and download images
        srcsets.map((ss, index) => {
            const parsed = srcset.parse(ss);
            if (!parsed) {
                return
            }
            const maxsrc = parsed.sort((a, b) => b.width - a.width)[0];
            const filename = saveId ? saveId + '-' + index + '.jpg' : index + '.jpg';
            download(maxsrc, filename);
            const photo = { url: maxsrc, filename };
            post.photos.push(photo);
            photos.push(photo);
        });
    }
}

module.exports = downloadInstagramPhotos;

/**
 * As Command Line Tool
 */

if (require.main === module) {
    const args = process.argv.slice(2);
    const url = args[0];

    const dataFile = 'instagram.json';
    let data;
    try {
        data = JSON.parse(fs.readFileSync(dataFile, 'utf8') || '{}');
    } catch (e) {
        data = {};
    }

    if (!args.length && !data.url) {
        console.log('Please specify a URL or @username!')
    } else {
        downloadInstagramPhotos(url || data.url, data)
            .then(data => fs.writeFileSync(dataFile, JSON.stringify(data)));
    }
}
