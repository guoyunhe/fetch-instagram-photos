# Download Instagram Photos

Download Instagram photos without API limitations.

## Please Don't Abuse This Software

**ONLY FOR PERSONAL USAGE!**

You can download photos and view them in your PC, without having an Instagram
account and being tracked by any individuals or organizations. For example, you
want to view photos of cute girls but don't want your girlfriend/wife to find it
in your Instagram timeline.

However, you should respect the copyright of content creators on Instagram.

**DO NOT:**

- Share or repost these photos on any online platforms.
- Use these photos in any your projects without asking permissions from authors.
- Share this tool without telling users the warnings above.

## How to use

1. Download and install [Node.js](https://nodejs.org/en/).
2. Run `sudo npm install -g fetch-instagram-photos`.
3. To download photos of someone, run:

   ```bash
   mkdir someone
   cd someone
   dl-ig https://www.instagram.com/someone/
   # or
   dl-ig @someone
   ```

4. To download photos of a post <https://www.instagram.com/p/BpWD99DF9rw/>, run:

   ```bash
   mkdir post
   cd post
   dl-ig https://www.instagram.com/p/BpWD99DF9rw/
   # or
   dl-ig BpWD99DF9rw
   ```

## Difference with other tools

1. Don't use Instagram Graph API. So here is no API limitations. Other tools
   using Instagram Graph API have limitation of requests per hour and might be
   blocked if some users abuse the tool.
2. Don't simply parse HTML. Other tools fetch and parse HTML. They can only get
   a single post and the first screen of user page. This tool simulate a full
   browser environment and navigate through out the whole user page and download
   all photos of public account.
3. Can synchronize account photos to local environment. Each photo has an ID and
   timestamp of last synchronization will be saved.
4. Won't be recognized as abuse and blocked. It is just a normal web browser for
   Instagram server.
